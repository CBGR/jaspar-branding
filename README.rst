JASPAR Logos
============

This distribution contains JASPAR logos for deployment at the official site
location. Since the logos included here are not redistributable, they should
not be deployed at other sites, since the use of some of the logos indicates a
collaboration or relationship between the JASPAR developers or service and the
originators of these logos.

Archival
--------

To make an archive of this distribution, use the following command in the base
directory of the repository:

.. code:: shell

  git archive --prefix=jaspar-branding/ --format=tar.gz -o ../jaspar-branding.tar.gz HEAD

Copyright and Licensing Information
-----------------------------------

See the ``.reuse/dep5`` file and the accompanying ``LICENSES`` directory in
this distribution for complete copyright and licensing information.
