The Elixir logo was made available in a suitable form at the following
locations:

https://raw.githubusercontent.com/elixir-no-nels/galaxy-tsd/master/assets/images/elixir_no_logo.png
https://raw.githubusercontent.com/elixir-no-nels/nels-docs/main/docs/images/elixir_norway_white_background.png

The licensing of the content in the parent repositories is inconsistent,
however, with the former bearing an MIT licence, and the latter bearing a
CC-BY-SA 4.0 licence.

It is more likely that the latter of these licences applies, but since there
might also be trademark-related restrictions, no licence can really be
assumed.
